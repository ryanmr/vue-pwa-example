const pages = [

  {
    slug: 'first-post',
    component: 'first-post'
  },
  {
    slug: 'second-post',
    component: 'second-post',
    offline: true
  },
  {
    slug: 'third-post',
    component: 'third-post'
  },
  {
    slug: 'fourth-post',
    component: 'fourth-post'
  }

]

const routes = pages.map(page => {
  const route = {
    name: `Page:${page.slug}`,
    path: `/pages/${page.slug}`,
    component: require(`@/pages/${page.slug}`),
    meta: {
      offline: page.offline ? page.offline : undefined
    }
  }
  return route
})

export default routes