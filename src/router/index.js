import Vue from 'vue'
import Router from 'vue-router'
import Home from '@/components/Home'
import pageRoutes from './pages'

Vue.use(Router)

const router = new Router({
  mode: 'history',
  routes: [
    {
      path: '/',
      name: 'Home',
      component: Home
    },
    ...pageRoutes
  ]
})

router.beforeEach((to, from, next) => {
  const online = navigator.onLine
  const offline = online === true
  const offlineRoute = to && to.meta && to.meta.offline === true
  const result = offline && offlineRoute
  if (!result) {
    next()
  } else {
    next({name: 'Home'})
  }
})

export default router